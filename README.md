# ELEL226_Proyecto2_Control_Temperatura_Fermentación

Repositorio de versiones para el proyecto "Control de temperatura de fermentación", correspondiente al proyecto final del ramo ELEL226.

## Respecto utilización:
Si se desea poner en práctica el sistema desarrollado, debe descargar las últimas versiones en este repositorio de los archivos Arduino y Processing. Ambos programas deben ser utilizados en conjunto, el programa de arduino al correr no cambiará de estados por la falta de señales en puerto serial, mientras que processing entregará error al no detectar la conexión con el puerto serial de arduino y su lienzo se congelará. Por esto es necesario tener el puerto serie de arduino conectado a la computadora, con identificación de puerto "COM3", antes de poner a prueba el programa de processing.

## Autores
Felipe Contreras - Francisco Navarrete - Tamara Peña.
